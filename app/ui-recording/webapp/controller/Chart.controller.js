// sap.ui.define([
// 	"sap/ui/core/mvc/Controller"
// ], function (Controller) {
// 	"use strict";

// 	return Controller.extend("uireporting.controller.Detail", {
// 		onInit: function () {
// 			var oOwnerComponent = this.getOwnerComponent();

// 			this.oRouter = oOwnerComponent.getRouter();
// 			this.oModel = oOwnerComponent.getModel();
//             debugger
// 			this.oRouter.getRoute("master").attachPatternMatched(this._onMemberMatched, this);
// 			this.oRouter.getRoute("detail").attachPatternMatched(this._onMemberMatched, this);
// 		},

// 		_onMemberMatched: function (oEvent) {
//             debugger
// 			this._member = oEvent.getParameter("arguments").member || this._member || "0";
// 			this.getView().bindElement({
// 				path: `/${this._member}`
// 			});
//             console.log(this.getView())
// 		},

// 		onEditToggleButtonPress: function() {
// 			var oObjectPage = this.getView().byId("ObjectPageLayout"),
// 				bCurrentShowFooterState = oObjectPage.getShowFooter();

// 			oObjectPage.setShowFooter(!bCurrentShowFooterState);
// 		},

// 		onExit: function () {
// 			this.oRouter.getRoute("list").detachPatternMatched(this._onMemberMatched, this);
// 			this.oRouter.getRoute("detail").detachPatternMatched(this._onMemberMatched, this);
// 		}
// 	});
// });

sap.ui.define([
	"sap/ui/core/mvc/Controller",
	'sap/ui/model/BindingMode',
	'sap/ui/model/json/JSONModel',
    'sap/viz/ui5/data/FlattenedDataset',
    'sap/viz/ui5/format/ChartFormatter',
    'sap/viz/ui5/api/env/Format'
], function (Controller, BindingMode, JSONModel,   FlattenedDataset,
    ChartFormatter,
    Format) {
	"use strict";
	

	return Controller.extend("uirecording.controller.Chart", {
		_milkdata : {
			"Chart": [{
				  "Week": "Week 1 - 4",
				  "Revenue": 431000.22,
				  "Cost": 230000.00,
				  "Cost1": 24800.63,
				  "Cost2": 205199.37,
				  "Cost3": 199999.37,
				  "Target": 500000.00,
				  "Budget": 210000.00
			  },
			  {
				  "Week": "Week 5 - 8",
				  "Revenue": 494000.30,
				  "Cost": 238000.00,
				  "Cost1": 99200.39,
				  "Cost2": 138799.61,
				  "Cost3": 200199.37,
				  "Target": 500000.00,
				  "Budget": 224000.00
			  },
			  {
				  "Week": "Week 9 - 12",
				  "Revenue": 491000.17,
				  "Cost": 221000.00,
				  "Cost1": 70200.54,
				  "Cost2": 150799.46,
				  "Cost3": 80799.46,
				  "Target": 500000.00,
				  "Budget": 238000.00
			  },
			  {
				  "Week": "Week 13 - 16",
				  "Revenue": 536000.34,
				  "Cost": 280000.00,
				  "Cost1": 158800.73,
				  "Cost2": 121199.27,
				  "Cost3": 108800.46,
				  "Target": 500000.00,
				  "Budget": 252000.00
			  },
			  {
				  "Week": "Week 17 - 20",
				  "Revenue": 675000.00,
				  "Cost": 230000.00,
				  "Cost1": 140000.91,
				  "Cost2": 89999.09,
				  "Cost3": 100099.09,
				  "Target": 600000.00,
				  "Budget": 266000.00
			  },
			  {
				  "Week": "Week 21 - 24",
				  "Revenue": 680000.00,
				  "Cost": 250000.00,
				  "Cost1": 172800.15,
				  "Cost2": 77199.85,
				  "Cost3": 57199.85,
				  "Target": 600000.00,
				  "Budget": 280000.00
			  },
			  {
				  "Week": "Week 25 - 28",
				  "Revenue": 659000.14,
				  "Cost": 325000.00,
				  "Cost1": 237200.74,
				  "Cost2": 87799.26,
				  "Cost3": 187799.26,
				  "Target": 600000.00,
				  "Budget": 294000.00
			  },
			  {
				  "Week": "Week 29 - 32",
				  "Revenue": 610000.00,
				  "Cost": 350000.00,
				  "Cost1": 243200.18,
				  "Cost2": 106799.82,
				  "Cost3": 206799.82,
				  "Target": 600000.00,
				  "Budget": 308000.00
			  },
			  {
				  "Week": "Week 33 - 37",
				  "Revenue": 751000.83,
				  "Cost": 390000.00,
				  "Cost1": 280800.24,
				  "Cost2": 109199.76,
				  "Cost3": 209199.76,
				  "Target": 600000.00,
				  "Budget": 322000.00
			  },
			  {
				  "Week": "Week 38 - 42",
				  "Revenue": 800000.63,
				  "Cost": 450000.00,
				  "Cost1": 320000.08,
				  "Cost2": 129999.92,
				  "Cost3": 409199.76,
				  "Target": 700000.00,
				  "Budget": 336000.00
			  },
			  {
				  "Week": "Week 43 - 47",
				  "Revenue": 881000.19,
				  "Cost": 480000.00,
				  "Cost1": 360800.09,
				  "Cost2": 119199.91,
				  "Cost3": 210109.01,
				  "Target": 700000.00,
				  "Budget": 350000.00
			  },
			  {
				  "Week": "Week 47 - 52",
				  "Revenue": 904000.04,
				  "Cost": 560000.00,
				  "Cost1": 403200.08,
				  "Cost2": 156799.92,
				  "Cost3": 139199.01,
				  "Target": 700000.00,
				  "Budget": 364000.00
			  }]
		  },

        settingsModel : {
            dataset : {
                name: "Dataset",
                defaultSelected : 1,
                values : [{
                    name : "Small",
                    value : this._milkdata
                },{
                    name : "Medium",
                    value : this._milkdata
                },{
                    name : "Large",
                    value : this._milkdata
                }]
            },
            series : {
                name : "Series",
                defaultSelected : 0,
                values : [{
                    name : "1 Series",
                    value : ["Revenue"]
                }, {
                    name : '2 Series',
                    value : ["Revenue", "Cost"]
                }]
            },
            dataLabel : {
                name : "Value Label",
                defaultState : true
            },
            axisTitle : {
                name : "Axis Title",
                defaultState : false
            },
            dimensions: {
                Small: [{
                    name: 'Seasons',
                    value: "{Seasons}"
                }],
                Medium: [{
                    name: 'Week',
                    value: "{Week}"
                }],
                Large: [{
                    name: 'Week',
                    value: "{Week}"
                }]
            },
            measures: [{
               name: 'Revenue',
               value: '{Revenue}'
            },{
               name: 'Cost',
               value: '{Cost}'
            }]
        },

        oVizFrame : null,

		onInit: function () {
			console.log("nothing")
			return
			var sScnJsonPath = jQuery.sap.getModulePath("uirecording", "/model/scnHelp.json");
            // var oJSONModel = new JSONModel(sJsonPath);
            // var oOtherJSONModel = new JSONModel(sOtherJsonPath);
			Format.numericFormatter(ChartFormatter.getInstance());
            var formatPattern = ChartFormatter.DefaultPattern;
            // set explored app's demo model on this sample
            var oModel = new JSONModel(this.settingsModel);
            oModel.setDefaultBindingMode(BindingMode.OneWay);
            this.getView().setModel(oModel);

            var oVizFrame = this.oVizFrame = this.getView().byId("idVizFrame");
            oVizFrame.setVizProperties({
                plotArea: {
                    dataLabel: {
                        formatString:formatPattern.SHORTFLOAT_MFD2,
                        visible: true
                    }
                },
                valueAxis: {
                    label: {
                        formatString: formatPattern.SHORTFLOAT
                    },
                    title: {
                        visible: false
                    }
                },
                categoryAxis: {
                    title: {
                        visible: false
                    }
                },
                title: {
                    visible: false,
                    text: 'Revenue by City and Store Name'
                }
            });
            var dataModel = new JSONModel(this._milkdata);
            //oVizFrame.setModel(dataModel);
           // oVizFrame.setModel(this.getView().getModel());
		},
	});
});