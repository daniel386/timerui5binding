/**
 * eslint-disable @sap/ui5-jsdocs/no-jsdoc
 */

sap.ui.define([
        "sap/ui/core/UIComponent",
        "sap/ui/Device",
        "uirecording/model/models",
        'sap/f/library'
    ],
    function (UIComponent, Device, models, fioriLibrary) {
        "use strict";

        return UIComponent.extend("uirecording.Component", {
            metadata: {
                manifest: "json"
            },

            /**
             * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
             * @public
             * @override
             */
            init: function () {
                var oRouter,
                oModel;
                // call the base component's init function
                UIComponent.prototype.init.apply(this, arguments);

                // enable routing
                //this.getRouter().initialize();
                oRouter = this.getRouter();
                oRouter.attachBeforeRouteMatched(this._onBeforeRouteMatched, this);
                oRouter.initialize();

                // set the device model
                this.setModel(models.createDeviceModel(), "device");
            },
            _onBeforeRouteMatched: function(oEvent) {
                // var oModel = this.getModel(),
                //     sLayout = oEvent.getParameters().arguments.layout;
    
                // // If there is no layout parameter, set a default layout (normally OneColumn)
                // if (!sLayout) {
                //     sLayout = fioriLibrary.LayoutType.OneColumn;
                // }
    
                // oModel.setProperty("/layout", sLayout);
            }
        });
    }
);