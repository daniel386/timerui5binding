using {
  Currency,
  managed,
  sap,
  cuid
} from '@sap/cds/common';

namespace de.hgh;

/** MasterData */

entity Members : managed, cuid {
  userName   : String;
  memberID   : String;
  firstName  : String;
  lastName   : String;
  email      : String;
  joinDate   : String;
  to_Chapter : Association to one Chapters;
  imageURL   : String;
  to_Chart   : Association to many Chart;
}

entity Chapters : managed, cuid {
  name          : String;
  establishDate : Date;
  image         : LargeBinary @Core.MediaType: 'image/png';
  to_Members    : Association to many Members
                    on to_Members.to_Chapter = $self;
}

/** Hierarchically organized Code List for Genres */
entity Genres : sap.common.CodeList {
  key ID       : Integer;
      parent   : Association to Genres;
      children : Composition of many Genres
                   on children.parent = $self;
}

/**  Transactional Data  */
// entity Operations : managed, cuid {
//   name      : String;
//   startDate : Date;
//   endDate   : Date;
//   to_SubOps : Association to many SubOps on to_SubOps.to_Operation = $self;
// }

// entity SubOps : managed, cuid { // Operations
//   name      : String;
//   to_Operation: Association to one Operations;
//   to_Activities : Association to many Activities on to_Activities.to_SubOp = $self;
// }

entity Activities : managed, cuid { // Operations
  name      : String;
  givesCash : Boolean;
  //to_SubOp: Association to one SubOps;
} 

entity Chart {
    key Week: String;
    Revenue: Decimal;
    Cost: Decimal;
    Cost1: Decimal;
    Cost2: Decimal;
    Cost3: Decimal;
    Target: Decimal;
    Budget: Decimal;
}

entity StaffingRecords: managed, cuid {
    to_Activity: Association to one Activities;
    to_Member: Association to one Members;
}

entity Entries : managed, cuid {
  description: String;
  start: Time;
  end: Time;
  duration: Decimal;
  to_Member: Association to one Members;
  to_Activity: Association to one Activities;
}



