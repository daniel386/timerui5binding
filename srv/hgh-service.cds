using { de.hgh as my } from '../db/schema';
service HGHService {

  entity Members as projection on my.Members;
  entity Activities as projection on my.Activities;
  entity Entries as projection on my.Entries;
  entity Chart as projection on my.Chart;

  // @requires: 'authenticated-user'
}
